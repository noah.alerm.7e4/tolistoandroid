package com.example.tolistoandroid.data.api

import com.example.tolistoandroid.data.models.Llista
import com.example.tolistoandroid.data.models.Task
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface Api {
    companion object {
        //API CALL
        val retrofit: Retrofit = Retrofit.Builder().baseUrl("https://tolistoapi.herokuapp.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
    }

    //LISTS

    //GETTER
    @GET("/lists")
    fun getLists(): Call<List<Llista>>

    //POST
    @POST("/lists")
    fun addList(@Body list: Llista): Call<Llista>

    //DELETE
    @DELETE("/lists/{id}")
    fun deleteList(@Path("id") id: Long): Call<Llista>

    //PUT
    @PUT("/lists/{id}")
    fun modifyList(@Path("id") id: Long, @Body list: Llista) : Call<Llista>


    //TASKS

    //GET
    @GET("/lists/{idList}/tasks")
    fun getTasks(@Path("idList") idList: Long): Call<List<Task>>

    //POST
    @POST("/lists/{idList}/tasks")
    fun addTask(@Path("idList") id: Long, @Body task: Task): Call<Task>

    //DELETE
    @DELETE("/lists/{idList}/tasks/{id}")
    fun deleteTask(@Path("idList") idList: Long, @Path("id") id: Long): Call<Task>

    //PUT
    @PUT("/lists/{idList}/tasks/{id}")
    fun modifyTask(@Path("idList") idList: Long, @Path("id") id: Long): Call<Task>

    @PUT("/lists/{idList}/tasks{id}")
    fun modifyTaskPosition(@Path("idList") idList: Long, @Path("id") id: Long, @Body task: Task): Call<Task>
}
