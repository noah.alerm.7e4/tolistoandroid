package com.example.tolistoandroid.data.models

data class Task(val id: Long?, val description: String, var done: Boolean, var position: Long, var listId: Long)
