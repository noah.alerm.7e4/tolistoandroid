package com.example.tolistoandroid.ui.views

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.transition.TransitionInflater
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.tolistoandroid.R
import com.example.tolistoandroid.data.models.Task
import com.example.tolistoandroid.ui.adapters.TasksAdapter
import com.example.tolistoandroid.ui.viewModel.ViewModel
import com.example.tolistoandroid.utils.PopupMenuMethods
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class TasksFragment : Fragment(R.layout.fragment_tasks) {
    //ATTRIBUTES
    private lateinit var goBackButton: ImageView
    private lateinit var listTitle: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var addTaskButton: FloatingActionButton
    private lateinit var menuButton: ImageView
    private lateinit var editButton: ImageView

    //ANIMATION ITEMS
    private var fabOpen: Animation? = null
    private var fabClose: Animation? = null
    private var rotateForward: Animation? = null
    private var rotateBackward: Animation? = null
    private var isOpen = false

    //view model
    private val viewModel: ViewModel by activityViewModels()

    //refresh
    private lateinit var swipeRefresh: SwipeRefreshLayout

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.comesFromSearch = false

        //TRANSITION
        if (!viewModel.comesFromSelf) {
            val inflater = TransitionInflater.from(requireContext())
            enterTransition = inflater.inflateTransition(R.transition.slide_right).setDuration(800)
        }
        viewModel.comesFromSelf = false

        //LIST
        val list = viewModel.getLists().filter { it.id == arguments?.getLong("listId")!! }[0]
        Log.d("LIST", list.toString())

        //TASKS
        var tasks = viewModel.getTasksFromAPI(list.id!!)

        //IDs
        goBackButton = view.findViewById(R.id.go_back_button)
        listTitle = view.findViewById(R.id.list_title)
        recyclerView = view.findViewById(R.id.recycler_view_tasks)
        addTaskButton = view.findViewById(R.id.add_task_button)
        menuButton = view.findViewById(R.id.menu_button)
        editButton = view.findViewById(R.id.edit_icon)
        swipeRefresh = view.findViewById(R.id.refresh)

        //REFRESH
        swipeRefresh.setOnRefreshListener {
            viewModel.getTasksFromAPI(list.id!!)

            //NAVIGATION
            Handler(Looper.getMainLooper()).postDelayed({
                viewModel.comesFromSelf = true
                val action = TasksFragmentDirections.actionTasksSelf(list.id!!)
                findNavController().navigate(action)
            }, 500)
        }

        //LIST TITLE
        listTitle.text = list.name

        //RECYCLER VIEW
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        Handler(Looper.getMainLooper()).postDelayed({
            recyclerView.adapter = TasksAdapter(tasks, list, viewModel)
        }, 500)

        //ANIMATION IDs
        fabOpen = AnimationUtils.loadAnimation(requireContext(), R.anim.fab_open)
        fabClose = AnimationUtils.loadAnimation(requireContext(), R.anim.fab_close)
        rotateForward = AnimationUtils.loadAnimation(requireContext(), R.anim.rotate_forward)
        rotateBackward = AnimationUtils.loadAnimation(requireContext(), R.anim.rotate_backward)

        //RECYCLER VIEW MANAGEMENT
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or
                ItemTouchHelper.DOWN or ItemTouchHelper.START or ItemTouchHelper.END, ItemTouchHelper.LEFT) {
            //MOVE
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target:
            RecyclerView.ViewHolder): Boolean {
                //the position from where item has been moved
                val from = viewHolder.bindingAdapterPosition

                //the position where the item is moved
                val to = target.bindingAdapterPosition

                //API POSITION UPDATE
                for (task in tasks) {
                    var newPosition = task.position

                    //MOVED TASK
                    if (task.position == from.toLong())
                        newPosition = to.toLong()
                    //UP MOTION
                    else if (to < from && task.position < from.toLong() && task.position >= to.toLong())
                        newPosition++
                    //DOWN MOTION
                    else if (to > from && task.position > from.toLong() && task.position <= to.toLong())
                        newPosition--

                    //PUT
                    val modifiedTask = Task(task.id, task.description, task.done, newPosition, task.listId)
                    viewModel.modifyTasksPositionInAPI(list.id!!, task.id!!, modifiedTask)
                }

                //LOCAL POSITION UPDATE
                for (task in list.tasks) {
                    //MOVED TASK
                    if (task.position == from.toLong())
                        task.position = to.toLong()
                    //UP MOTION
                    else if (to < from && task.position < from.toLong() && task.position >= to.toLong())
                        task.position++
                    //DOWN MOTION
                    else if (to > from && task.position > from.toLong() && task.position <= to.toLong())
                        task.position--
                }

                //telling the adapter to move the item
                recyclerView.adapter?.notifyItemMoved(from, to)

                //TASKS UPDATE
                Handler(Looper.getMainLooper()).postDelayed({
                    tasks = viewModel.getTasksFromAPI(list.id!!)
                }, 700)
                Log.d("POSITION UPDATE", tasks.toString())

                return true
            }

            //SWIPE
            override fun onSwiped(h: RecyclerView.ViewHolder, dir: Int) {
                val position = h.bindingAdapterPosition
                val item = tasks.filter { t -> t.position == position.toLong() }[0] //This is used to obtain only the task whose position is the one modified in the adapter.
                val itemPosition = item.position

                //DELETE
                viewModel.deleteTaskFromAPI(list.id!!, item.id!!)

                //API POSITION UPDATE
                for (task in tasks) {
                    if (task.position > itemPosition) {
                        //PUT
                        val modifiedTask = Task(task.id, task.description, task.done, task.position-1, task.listId)
                        viewModel.modifyTasksPositionInAPI(list.id!!, task.id!!, modifiedTask)
                    }
                }

                //LOCAL POSITION UPDATE
                for (task in list.tasks) {
                    if (task.position > itemPosition)
                        task.position--
                }

                //Local update
                list.tasks.remove(item)

                //Recycler view update
                (recyclerView.adapter as TasksAdapter).notifyItemRemoved(position)

                //NAVIGATION
                viewModel.comesFromSelf = true
                val action = TasksFragmentDirections.actionTasksSelf(list.id!!)
                Navigation.findNavController(view).navigate(action)

                //SNACKBAR (UNDO)
                val snackbar = Snackbar.make(recyclerView, "${item.description} was removed from the list", Snackbar.LENGTH_LONG)
                snackbar.setAction("UNDO") {
                    //NEW TASK
                    val undoneTask = Task(null, item.description, item.done, item.position, item.listId)

                    //Local update
                    list.tasks.add(undoneTask)

                    //POST
                    viewModel.addTaskToAPI(list.id!!, undoneTask)
                    Log.d("UNDO TEST", item.toString())

                    //Recycler view update
                    (recyclerView.adapter as TasksAdapter).notifyItemInserted(position)
                    recyclerView.scrollToPosition(position)

                    //NAVIGATION
                    viewModel.comesFromSelf = true
                    val action = TasksFragmentDirections.actionTasksSelf(list.id!!)
                    findNavController().navigate(action)
                }
                snackbar.setActionTextColor(ContextCompat.getColor(requireContext(),
                    R.color.secondary_color_dark
                ))
                snackbar.show()
            }

            //CHILD DRAW
            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                                     dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

                //RED BACKGROUND
                var bg = ContextCompat.getDrawable(requireContext(), R.drawable.swipe_to_delete_bg)
                bg?.bounds = Rect(viewHolder.itemView.right + dX.toInt()-100, viewHolder.itemView.top,
                    viewHolder.itemView.right, viewHolder.itemView.bottom)
                if (dY == 0F)
                    bg?.draw(c)

                //NORMAL BACKGROUND
                //We need to reset the background due to the view's rounded corners.
                if (dX == 0F) {
                    bg = ContextCompat.getDrawable(requireContext(), R.color.secondary_color_dark)
                    bg?.bounds = Rect(viewHolder.itemView.right + dX.toInt()-100, viewHolder.itemView.top,
                        viewHolder.itemView.right, viewHolder.itemView.bottom)
                    bg?.draw(c)
                }


                //SWIPE DECORATOR (see Gradle Implementation)
                RecyclerViewSwipeDecorator.Builder(requireContext(), c, recyclerView, viewHolder, dX,
                    dY, actionState, isCurrentlyActive)
                    .addActionIcon(R.drawable.trash_can)
                    .create().decorate()
            }
        }).attachToRecyclerView(recyclerView)


        //ON CLICK
        //go back button
        goBackButton.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_tasks_to_lists)
        }

        //menu button
        menuButton.setOnClickListener { v: View ->
            PopupMenuMethods.showMenu(v, R.menu.popup_menu, requireContext(), true)
        }

        //edit button
        editButton.setOnClickListener {
            //CUSTOM TITLE
            val title = TextView(requireContext())
            title.text  = "EDIT LIST"
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
            title.gravity = Gravity.CENTER
            title.setTextColor(ContextCompat.getColor(requireContext(), R.color.text))
            title.typeface = Typeface.DEFAULT_BOLD
            title.background = AppCompatResources.getDrawable(requireContext(),
                R.drawable.add_task_dialog_title_bg
            )
            title.setPadding(0, 30, 0, 30)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.AddTaskDialogTheme)
            val viewDialog = layoutInflater.inflate(R.layout.custom_edit_list_dialog,null)
            builder.setView(viewDialog)
            builder.setCustomTitle(title)
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("EDIT") { _, _ ->
                //ATTRIBUTES
                val textInput: EditText = viewDialog.findViewById(R.id.edit_list_name)

                if (textInput.text.toString() != "") {
                    //Local update
                    list.name = textInput.text.toString()

                    //PUT
                    viewModel.modifyListInAPI(list)

                    //NAVIGATION
                    viewModel.comesFromSelf = true
                    val action = TasksFragmentDirections.actionTasksSelf(list.id!!)
                    Navigation.findNavController(view).navigate(action)
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.show()
        }

        //add button
        addTaskButton.setOnClickListener {
            //CUSTOM TITLE
            val title = TextView(requireContext())
            title.text = "NEW TASK"
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
            title.gravity = Gravity.CENTER
            title.setTextColor(ContextCompat.getColor(requireContext(), R.color.text))
            title.typeface = Typeface.DEFAULT_BOLD
            title.background = AppCompatResources.getDrawable(requireContext(),
                R.drawable.add_task_dialog_title_bg
            )
            title.setPadding(0, 30, 0, 30)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.AddTaskDialogTheme)
            val viewDialog = layoutInflater.inflate(R.layout.custom_add_task_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(title)
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("ADD") { _, _ ->
                //ATTRIBUTES
                val textInput: EditText = viewDialog.findViewById(R.id.add_task_description)

                if (textInput.text.toString() != "") {
                    //NEW TASK
                    val newTask = Task(null, textInput.text.toString(), false, tasks.size.toLong(), list.id!!)

                    //Local update
                    list.tasks.add(newTask)

                    //POST
                    viewModel.addTaskToAPI(list.id!!, newTask)

                    //Recycler view update
                    (recyclerView.adapter as TasksAdapter).notifyItemInserted(list.tasks.size)

                    //NAVIGATION
                    viewModel.comesFromSelf = true
                    val action = TasksFragmentDirections.actionTasksSelf(list.id!!)
                    findNavController().navigate(action)
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.show()

            //ANIMATION
            animateFab()
        }
    }

    //METHODS
    /**
     * This method is used to add an animation to the add button.
     */
    private fun animateFab() {
        isOpen = if (isOpen) {
            addTaskButton.startAnimation(rotateForward)
            false
        } else {
            addTaskButton.startAnimation(rotateForward)
            true
        }
    }
}
