package com.example.tolistoandroid.ui.viewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.tolistoandroid.data.api.Api
import com.example.tolistoandroid.data.models.Llista
import com.example.tolistoandroid.data.models.Task
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewModel : ViewModel() {
    //ATTRIBUTES
    private var lists = mutableListOf<Llista>()

    //booleans
    var isFirstTime = true
    var comesFromSelf = false
    var comesFromSearch = false

    //GETTERS
    fun getLists() = lists

    //METHODS

    //lists
    /**
     * This method is used to obtain all lists from the API.
     */
    fun getListsFromAPI() {
        val call = Api.retrofit.create(Api::class.java).getLists()

        call.enqueue(object : Callback<List<Llista>> {
            //RESPONSE
            override fun onResponse(call: Call<List<Llista>>, response: Response<List<Llista>>) {
                if (response.isSuccessful) {
                    lists.clear()
                    lists.addAll(response.body()!!.toMutableList())

                    Log.d("LISTS (GET)", lists.toString())
                }
            }

            //FAILURE
            override fun onFailure(call: Call<List<Llista>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
        })
    }

    /**
     * This method is used to add a new list to the API.
     * @param newList New List
     */
    fun addListToAPI(newList: Llista) {
        Api.retrofit.create(Api::class.java).addList(newList).enqueue(object : Callback<Llista> {
            //RESPONSE
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                Log.i("SUCCESS", "The post has been successful.")
            }

            //FAILURE
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", "The post has NOT been successful.")
            }
        })
    }

    /**
     * This method is used to delete a list from the API.
     * @param id The ID of the list to be deleted
     */
    fun deleteListFromAPI(id: Long) {
        Api.retrofit.create(Api::class.java).deleteList(id).enqueue(object : Callback<Llista> {
            //RESPONSE
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                Log.i("SUCCESS", "The delete has been successful.")
            }

            //FAILURE
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", "The delete has NOT been successful.")
            }
        })
    }

    /**
     * This method is used to modify a lists name in the API.
     * @param list List to modify
     */
    fun modifyListInAPI(list: Llista) {
        Api.retrofit.create(Api::class.java).modifyList(list.id!!, list).enqueue(object :
            Callback<Llista> {
            //RESPONSE
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                Log.i("SUCCESS", "The put has been successful.")                        }

            //FAILURE
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                Log.e("ERROR", "The put has NOT been successful.")
            }
        })
    }

    //tasks
    /**
     * This method is used to obtain a list's tasks from the API.
     * @param idList The ID of the list that contains the tasks.
     */
    fun getTasksFromAPI(idList: Long) : List<Task> {
        val tasks = mutableListOf<Task>()

        Api.retrofit.create(Api::class.java).getTasks(idList).enqueue(object : Callback<List<Task>> {
            override fun onResponse(call: Call<List<Task>>, response: Response<List<Task>>) {
                if (response.isSuccessful) {
                    tasks.clear()
                    tasks.addAll(response.body()!!.toMutableList())

                    Log.d("TASKS (GET)", tasks.toString())
                }
            }

            override fun onFailure(call: Call<List<Task>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())            }
        })

        return tasks
    }

    /**
     * This method is used to add a new task to the API.
     * @param listId The ID of the list that will contain the task.
     * @param newTask New Task
     */
    fun addTaskToAPI(listId: Long, newTask: Task) {
        Api.retrofit.create(Api::class.java).addTask(listId, newTask).enqueue(object :
            Callback<Task> {
            //RESPONSE
            override fun onResponse(call: Call<Task>, response: Response<Task>) {
                Log.i("SUCCESS", "The post has been successful.")
            }

            //FAILURE
            override fun onFailure(call: Call<Task>, t: Throwable) {
                Log.e("ERROR", "The post has NOT been successful.")
            }
        })
    }

    /**
     * This method is used to delete a task from the API.
     * @param listId The ID of the list that contains the task.
     * @param id The ID of the task to be deleted.
     */
    fun deleteTaskFromAPI(listId: Long, id: Long) {
        Api.retrofit.create(Api::class.java).deleteTask(listId, id).enqueue(object : Callback<Task> {
            //RESPONSE
            override fun onResponse(call: Call<Task>, response: Response<Task>) {
                Log.i("SUCCESS", "The delete has been successful.")
                Log.d("DELETED ID", id.toString())
            }

            //FAILURE
            override fun onFailure(call: Call<Task>, t: Throwable) {
                Log.e("ERROR", "The delete has NOT been successful.")
            }
        })
    }

    /**
     * This method is used to check or uncheck a task in the API.
     * @param listId The ID of the list that contains the task.
     * @param id The ID of the task to be modified.
     */
    fun modifyTaskInAPI(listId: Long, id: Long) {
        Api.retrofit.create(Api::class.java).modifyTask(listId, id).enqueue(object : Callback<Task> {
            override fun onResponse(call: Call<Task>, response: Response<Task>) {
                Log.i("SUCCESS", "The put has been successful.")
            }

            override fun onFailure(call: Call<Task>, t: Throwable) {
                Log.e("ERROR", "The delete has NOT been successful.")
            }
        })
    }

    /**
     * This method is used to change a task's position in the API.
     * @param listId The ID of the list that contains the task.
     * @param task Task to be modified.
     */
    fun modifyTasksPositionInAPI(listId: Long, id: Long, task: Task) {
        Api.retrofit.create(Api::class.java).modifyTaskPosition(listId, id, task).enqueue(object : Callback<Task> {
            override fun onResponse(call: Call<Task>, response: Response<Task>) {
                Log.i("SUCCESS", "The put has been successful.")
            }

            override fun onFailure(call: Call<Task>, t: Throwable) {
                Log.e("ERROR", "The delete has NOT been successful.")
            }
        })
    }
}
