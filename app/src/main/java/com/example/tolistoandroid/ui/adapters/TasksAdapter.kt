package com.example.tolistoandroid.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tolistoandroid.R
import com.example.tolistoandroid.data.models.Llista
import com.example.tolistoandroid.data.models.Task
import com.example.tolistoandroid.ui.viewModel.ViewModel

class TasksAdapter(private val tasks: List<Task>, private val list: Llista, private val viewModel: ViewModel) :
    RecyclerView.Adapter<TasksAdapter.TasksViewHolder>() {
    //ADAPTER METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TasksViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.task_recyclerview_item,
            parent, false)

        return TasksViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    override fun onBindViewHolder(holder: TasksViewHolder, position: Int) {
        holder.bindData(tasks.filter { t -> t.position == position.toLong() }[0], list, viewModel)
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return tasks.size
    }


    //VIEW HOLDER
    class TasksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var description: TextView = itemView.findViewById(R.id.description)
        private var doneCheck: CheckBox = itemView.findViewById(R.id.done_check)

        //METHODS
        /**
         * This method is used to set up the data of each item of the tasks' list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(task: Task, list: Llista, viewModel: ViewModel) {
            description.text = task.description

            if (task.done)
                doneCheck.isChecked = true

            //ON CLICK
            doneCheck.setOnClickListener {
                //Local Update
                task.done = !task.done

                viewModel.modifyTaskInAPI(list.id!!, task.id!!)
            }
        }
    }
}
