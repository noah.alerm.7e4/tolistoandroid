package com.example.tolistoandroid.ui.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.tolistoandroid.R
import com.example.tolistoandroid.utils.PopupMenuMethods


class MenuFragment : Fragment(R.layout.fragment_menu) {
    //ATTRIBUTES
    private lateinit var buttonListMenu: Button
    private lateinit var buttonWhoMenu: Button
    private lateinit var buttonHowMenu: Button

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //TRANSITION
        view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in))

        //IDs
        buttonListMenu = view.findViewById(R.id.button_list_menu)
        buttonWhoMenu = view.findViewById(R.id.button_who_menu)
        buttonHowMenu  =view.findViewById(R.id.button_how_menu)

        //ON CLICK
        //lists
        buttonListMenu.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.action_menu_to_lists)
        }

        //about us
        buttonWhoMenu.setOnClickListener{
            PopupMenuMethods.dialogWho(requireContext())
        }

        //help
        buttonHowMenu.setOnClickListener{
            PopupMenuMethods.dialogHow(requireContext())
        }
    }
}
