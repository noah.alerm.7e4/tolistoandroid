package com.example.tolistoandroid.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.tolistoandroid.R
import com.example.tolistoandroid.data.models.Llista
import com.example.tolistoandroid.ui.views.ListsFragmentDirections


class ListsAdapter(private val lists: List<Llista>) : RecyclerView.Adapter<ListsAdapter.ListViewHolder>() {
    //ADAPTER METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.list_recyclerview_item,
            parent, false)

        return ListViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bindData(lists[position])

        //ON CLICK
        //navigation
        holder.itemView.setOnClickListener {
            //NAVIGATION TO TASKS
            val action = ListsFragmentDirections.actionListsToTasks(lists[position].id!!)
            Navigation.findNavController(holder.itemView).navigate(action)
        }
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return lists.size
    }


    //VIEW HOLDER
    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var name: TextView = itemView.findViewById(R.id.list_name)
        private var tasksSize: TextView = itemView.findViewById(R.id.task_num)

        //METHODS
        /**
         * This method is used to set up the data of each item of the lists' list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(list: Llista) {
            name.text = list.name
            tasksSize.text = "${list.tasks.size} tasks"
        }
    }
}
